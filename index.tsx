import * as React from "react";

import ReactDom from "react-dom";
import { App } from "./app";
import { DateBar } from "./DateBar";
import { Coustomer } from "./Coustomer";
const InvoiceApp = () => {
  return (
    <div>
      <App />
      <DateBar />
      <Coustomer />
    </div>
  );
};
ReactDom.render(<InvoiceApp />, document.getElementById("root"));
