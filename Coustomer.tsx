import * as React from "react";
import "./Coustomer.css";
export const Coustomer = () => {
  return (
    <div className="customerAmmountBar">
      <div className="companyInfo">
        <div>
          <b>Customer Company SA</b>
        </div>
        <div>100, Boulevard Royal, Brussels, Belgium</div>
        <div>IBAN: LUXXXXXXXX</div>
        <div>BIC: XXXXXX</div>
      </div>

      <div>
        <div className="totalAmount">
          <div>
            <div>Total amount spent:</div>
            <div>Total amount received:</div>
          </div>
          <div className="amount">
            <div>€4,135.00</div>
            <div>€1,500.60</div>
          </div>
        </div>
        <div className="totalDue">
          <div>
            <div>Total amount due:</div>
            <div>Please pay no later then:</div>
          </div>
          <div className="amount">
            <div>€ 2,684.00</div>
            <div>May 15th, 2020</div>
          </div>
        </div>
      </div>
    </div>
  );
};
