import * as React from "react";
import "./App.css";

export const App = () => {
  return (
    <div>
      <div className="titleBar">
        <div className="logo">EVEREST</div>
        <div className="contact">
          <div>www.everestcard.com</div>
          <div>+352 203 31027</div>
        </div>
      </div>
    </div>
  );
};
