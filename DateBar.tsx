import * as React from "react";
import "./DateBar.css";
export const DateBar = () => {
  return (
    <div className="dateBar">
      <div className="monthly">
        Monthly statement April 10, 2020 - May 9, 2020
      </div>
      <div className="page">Page 1 of 1</div>
    </div>
  );
};
